import React, { Component } from 'react';
import {
  Text,
  View,
  ListView,
  Button,
  StyleSheet,
  ScrollView,
  ToastAndroid,
  Platform,
  AlertIOS
} from 'react-native';

import { StackNavigator } from 'react-navigation';
import Collapsible from 'react-native-collapsible';

import SeminarScreen from './SeminarScreen';
import stockData from './data/stocks';

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.state = {
      dataSourceInitial: ds.cloneWithRows([stockData[0]]),
      dataSourceIndex: ds.cloneWithRows([stockData[0]]),
      dataSourceFavorites: ds.cloneWithRows([stockData[0]]),
      dataSourceFull: ds.cloneWithRows([stockData[0]]),
      isCollapse: true
    };
  }

  componentWillMount() {
    // this.startTime = new Date().getTime();
  }

  componentWillUpdate() {
    this.startTime = new Date().getTime();
  }

  render () {
    _stockChangeColor = (change) => {
      if (change.charAt(0) == '+') {
        return {
          fontSize: 25,
          color: 'green'
        }
      } else {
        return {
          fontSize: 25,
          color: 'red'
        }
      }
    }

    _stockBackgroundColor = (rowId) => {
      if (rowId % 2 == 0) {
        return {
          flex: 1, 
          flexDirection: 'row', 
          justifyContent: 'space-between', 
          padding: 8,
          backgroundColor: 'salmon'
        }
      } else {
        return {
          flex: 1, 
          flexDirection: 'row', 
          justifyContent: 'space-between', 
          padding: 8,
          backgroundColor: '#F4BABA'
        }
      }
    }

    _stockBackgroundColorFull = (rowId) => {
      if (rowId % 2 == 0) {
        return {
          flex: 1, 
          flexDirection: 'row', 
          justifyContent: 'space-between', 
          padding: 8,
          backgroundColor: '#F4BABA'
        }
      } else {
        return {
          flex: 1, 
          flexDirection: 'row', 
          justifyContent: 'space-between', 
          padding: 8,
          backgroundColor: 'salmon'
        }
      }
    }

    return (
      <View>
        <ScrollView>
          <ListView
            enableEmptySections={true}
            style={styles.list}
            dataSource={this.state.dataSourceInitial}
            renderRow={this._renderRowInitial}
            renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
          />
          <Collapsible collapsed={this.state.isCollapse} duration={1000}>
            <View style={styles.page}>
              <ListView
                enableEmptySections={true}
                style={styles.list}
                dataSource={this.state.dataSourceFull}
                renderRow={this._renderRowFull}
                renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
              />
            </View>
          </Collapsible>
          <Button title={this._buttonToggle(this.state.isCollapse)} onPress={()=> this.setState({isCollapse: !this.state.isCollapse})} />
          
          <View>
            <Text style={styles.indexTitle}>Index</Text>
            <ListView
              enableEmptySections={true}
              style={styles.list}
              dataSource={this.state.dataSourceIndex}
              renderRow={this._renderRowInitial}
              renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
            />
          </View>

          <View>
            <Text style={styles.favoritesTitle}>Favorites</Text>
            <ListView
              enableEmptySections={true}
              style={styles.list}
              dataSource={this.state.dataSourceFavorites}
              renderRow={this._renderRowInitial}
              renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
            />
          </View>
        </ScrollView>
      </View>
    );
  }

  _buttonToggle(state) {
    if (state) {
      return 'Show More'
    } else {
      return 'Show Less'
    }
  }

  _renderRowInitial(stockData, sectionId, rowId){
    return (
      <View style={this._stockBackgroundColor(rowId)}>
        <View>
          <Text style={styles.stockTitle}>{stockData.STOCK}</Text>
          <Text style={styles.stockName}>{stockData.Name}</Text>
        </View>
        <View>
          <Text style={this._stockChangeColor(stockData.Change)}>{stockData.Change}</Text>
          <Text style={styles.stockPrice}>{stockData.Price}</Text>
        </View>
      </View>
    )
  }

  _renderRowFull(stockData, sectionId, rowId){
    return (
      <View style={this._stockBackgroundColorFull(rowId)}>
        <View>
          <Text style={styles.stockTitle}>{stockData.STOCK}</Text>
          <Text style={styles.stockName}>{stockData.Name}</Text>
        </View>
        <View>
          <Text style={this._stockChangeColor(stockData.Change)}>{stockData.Change}</Text>
          <Text style={styles.stockPrice}>{stockData.Price}</Text>
        </View>
      </View>
    )
  }

  componentDidMount() {
    this.startTime = new Date().getTime();
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.setState({
      dataSourceInitial: ds.cloneWithRows([stockData[0],stockData[1],stockData[2]]),
      dataSourceIndex: ds.cloneWithRows([stockData[3],stockData[4],stockData[6]]),
      dataSourceFavorites: ds.cloneWithRows([stockData[7],stockData[8],stockData[9]]),
      dataSourceFull: ds.cloneWithRows(stockData.slice(3, stockData.length)),
    });
  }

  componentDidUpdate() {
    this.endTime = new Date().getTime();
    if (Platform.OS === 'ios') {
      AlertIOS.alert(Math.abs(this.startTime - this.endTime).toString() + ' Milliseconds');
    } else {
      ToastAndroid.show(Math.abs(this.startTime - this.endTime).toString() + ' Milliseconds', ToastAndroid.SHORT);
    }
  }
}

const AppNav = StackNavigator ({
  Home: { screen: HomeScreen},
  Seminar: { screen: SeminarScreen}
})

HomeScreen.navigationOptions = ({ navigation }) => ({
    title: "Dashboard",
    headerRight: 
    <View style={styles.headerButton}>
      <Button color='salmon' onPress={() => navigation.navigate('Seminar')} title='Seminars'/>
    </View>
});

const styles = StyleSheet.create({
  separator: {
    height: 1,
    backgroundColor: 'black'
  },
  list: {
    backgroundColor: 'salmon'
  },
  page: {
    backgroundColor: 'white'
  },
  headerButton: {
    marginRight: 10
  },
  stockTitle: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'black'
  },
  stockName: {
    fontSize: 18,
  },
  stockPrice: {
    fontSize: 18,
    alignSelf: 'flex-end'
  },
  indexTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
    padding: 15
  },
  favoritesTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
    padding: 15
  }
});

module.exports = HomeScreen;
module.exports = AppNav;