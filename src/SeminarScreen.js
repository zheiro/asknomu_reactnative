import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  Button,
  ListView,
  ScrollView,
  StyleSheet,
  ToastAndroid,
  Platform,
  AlertIOS
} from 'react-native';

import { Card } from 'react-native-material-design';
import Collapsible from 'react-native-collapsible';

import seminarData from './data/seminars';

class SeminarScreen extends Component {
  constructor(props) {
    super(props);
    // const ds = new ListView.DataSource({
    //   rowHasChanged: (r1, r2) => r1 !== r2
    // });
    this.state = {
      // dataSource: ds.cloneWithRows(seminarData),
      isCollapse1: true,
      isCollapse2: true
    }
  }

  componentWillMount() {
    this.startTime = new Date().getTime();
  }

  _buttonToggle(state) {
    if (state) {
      return 'Show Description'
    } else {
      return 'Hide Description'
    }
  }

  render () {
    return (
      <ScrollView>
        <Card>
          <Card.Media image={<Image source={require('../images/workshop.jpg')} />} 
            overlay
          />
          <Card.Body>
            <View style={{flexDirection: 'row', flex: 5}}>
              <View style={{flex:4}}>
                <Text style={styles.seminarTitle}> {seminarData[0].title} </Text>
                <Text style={styles.seminarSpeaker}> {seminarData[0].speaker} </Text>
                <Text style={styles.seminarVenue}> {seminarData[0].date} </Text>
                <Text style={styles.seminarDate}> {seminarData[0].venue} </Text>
              </View>
              <View style={{flex: 1}}>
                <Button title='Attend' color='salmon' />
              </View>
            </View>
            <View>
              <Collapsible collapsed={this.state.isCollapse1}>
                <View style={styles.seminarDecriptionView}>
                  <Text> {seminarData[0].description} </Text>
                </View>
              </Collapsible>
            </View>
          </Card.Body>
          <Button title={this._buttonToggle(this.state.isCollapse1)} onPress={()=> this.setState({isCollapse1: !this.state.isCollapse1})} />
        </Card>
        
        <Card>
          <Card.Media image={<Image source={require('../images/seminar.jpg')} />} 
            overlay
          />
          <Card.Body>
            <View style={{flexDirection: 'row', flex: 5}}>
              <View style={{flex:4}}>
                <Text style={styles.seminarTitle}> {seminarData[1].title} </Text>
                <Text style={styles.seminarSpeaker}> {seminarData[1].speaker} </Text>
                <Text style={styles.seminarVenue}> {seminarData[1].date} </Text>
                <Text style={styles.seminarDate}> {seminarData[1].venue} </Text>
              </View>
              <View style={{flex: 1}}>
                <Button title='Attend' color='salmon' />
              </View>
            </View>
            <View>
              <Collapsible collapsed={this.state.isCollapse2}>
                <View style={styles.seminarDecriptionView}>
                  <Text> {seminarData[1].description} </Text>
                </View>
              </Collapsible>
            </View>
          </Card.Body>
          <Button title={this._buttonToggle(this.state.isCollapse2)} onPress={()=> this.setState({isCollapse2: !this.state.isCollapse2})} />
        </Card>
      </ScrollView>
    )
  }

  /*render () {
    return (
      <View>
        <ScrollView>
          <ListView
            dataSource={this.state.dataSource}
            renderRow={this._renderCard.bind(this)}
            renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
          />
          <Button title={'Collapse'} onPress={() => this.setState({isCollapse: !this.state.isCollapse})} />
        </ScrollView>
      </View>
    );
  }

  _renderCard(seminarData, sectionId, rowId) {
    return (
      <Card>
        <Card.Media image={<Image source={require('../images/workshop.jpg')} />} 
          overlay
        />
        <Card.Body>
          <View>
            <View>
              <Text> {seminarData.title} </Text>
              <Text> {seminarData.speaker} </Text>
              <Text> {seminarData.venue} </Text>
              <Text> {seminarData.date} </Text>
            </View>
            <Collapsible collapsed={this.state.isCollapse}>
              <View>
                <Text> {seminarData.description} </Text>
              </View>
            </Collapsible>
            <View>
              <Button title='Attend'/>
            </View>
          </View>
        </Card.Body>
      </Card>
    )
  }*/

  componentDidMount() {
    this.endTime = new Date().getTime();
    if (Platform.OS === 'ios') {
      AlertIOS.alert(Math.abs(this.startTime - this.endTime).toString() + ' Milliseconds');
    } else {
      ToastAndroid.show(Math.abs(this.startTime - this.endTime).toString() + ' Milliseconds', ToastAndroid.SHORT);
    }
  }
}

SeminarScreen.navigationOptions = ({navigation}) => ({
   title: 'Seminars',
});

const styles = StyleSheet.create({
  separator: {
    height: 1,
    backgroundColor: 'black'
  },
  seminarTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black'
  },
  seminarSpeaker: {
    fontSize: 15,
  },
  seminarVenue: {
    fontSize: 15,
  },
  seminarDate: {
    fontSize: 15,
  },
  seminarDecriptionView: {
    marginTop: 10
  }

});

module.exports = SeminarScreen;