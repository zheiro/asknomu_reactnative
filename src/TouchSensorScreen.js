import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  View,
  Text
} from 'react-native'

class TouchSensorScreen extends Component {
  render () {
    return (
      <View style={styles.imageContainer}>
        <Image style={styles.image} source={require('../images/fingerprint.png')} />
        <Text style={styles.text}> Touch sensor to verify identity </Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  imageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'salmon'
  },
  image: {
    width: 100,
    height: 100,
    tintColor: 'white'
  },
  text: {
    marginTop: 20,
    fontFamily: 'Helvetica',
    color: 'white',
    fontSize: 15
  }
});

module.exports = TouchSensorScreen;