'use strict';
import React, { Component } from 'react';
import {
  ToastAndroid,
  Text,
  AppRegistry,
  AlertIOS
} from 'react-native';

import TouchID from 'react-native-touch-id';

import HomeScreen from './src/HomeScreen';
import AppNav from './src/HomeScreen';
import TouchSensorScreen from './src/TouchSensorScreen';

class LoginScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      isAuthorized : false,
    }
  }

  componentWillMount() {
    this.startTime = new Date().getTime();
  }

   touchAuth() {
    TouchID.authenticate('to demo this react-native component')
      .then(success => {
        AlertIOS.alert('Authenticated Successfully');
        this.setState({isAuthorized: true});
      })
      .catch(error => {
        AlertIOS.alert('Authentication Failed');
      });
   }
 
  render() {
    if (this.state.isAuthorized) {
      return <HomeScreen />
    } else {
      return <TouchSensorScreen />
    }
  }

  componentDidMount() {
    this.touchAuth();
    
    this.endTime = new Date().getTime();
    AlertIOS.alert(Math.abs(this.startTime - this.endTime).toString() + ' Milliseconds');
  }
 
  componentWillUnmount(){
    Finger.dismiss()
  }
}

AppRegistry.registerComponent('askNomura', () => LoginScreen);