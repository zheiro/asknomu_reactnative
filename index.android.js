'use strict';
import React, { Component } from 'react';
import {
  ToastAndroid,
  Text,
  AppRegistry
} from 'react-native';

import Finger from 'react-native-touch-id-android'

import HomeScreen from './src/HomeScreen';
import AppNav from './src/HomeScreen';
import TouchSensorScreen from './src/TouchSensorScreen';

class LoginScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      isAuthorized : false,
    }
  }

  componentWillMount() {
    this.startTime = new Date().getTime();
  }
 
  touchAuth(){
    Finger.requestTouch()
      .then(success => {
        // ToastAndroid.show('Identity verified', ToastAndroid.SHORT);
        this.setState({isAuthorized: true});
      })
      .catch(error => {
        ToastAndroid.show(error, ToastAndroid.SHORT);
        this.touchAuth();
      });
  }
 
  render() {
    if (this.state.isAuthorized) {
      return <HomeScreen />
    } else {
      return <TouchSensorScreen />
    }
  }

  componentDidMount() {
    Finger.isSensorAvailable()
      .then((isAvailable) => {
        // ToastAndroid.show('Sensor is available', ToastAndroid.SHORT);
        this.touchAuth();
      })
      .catch(error => {
        ToastAndroid.show(error, ToastAndroid.SHORT);
      });
    
    this.endTime = new Date().getTime();
    ToastAndroid.show(Math.abs(this.startTime - this.endTime).toString() + ' Milliseconds', ToastAndroid.SHORT);
  }
 
  componentWillUnmount(){
    Finger.dismiss()
  }
}

AppRegistry.registerComponent('askNomura', () => LoginScreen);